Mysql-CRUD api designed using NODEJS.

# Mysql_CRUD

- Simple RESTful API to
  retrieve data from the server (GET), create new data to the server (POST),
  update data to the server (PUT), and delete data to the server (DELETE)
  from a table in the database namely Customers.

- Simple RESTful API to
  retrieve data from the server (GET), create new data to the server (POST),
  update data to the server (PUT), and delete data to the server (DELETE)
  from a table in the database namely ​ product​ .

- API to get the list of all the customer who have purchased a particular item.
  API to get all products list purchased by a specific customer.
  API to get the list of most purchased products in descending order.

## Development server

Run `npm run dev` to start server.

Author: 'Nitish Kumar'
