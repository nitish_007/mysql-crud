const express = require("express");
const morgan = require("morgan");
const dotenv = require("dotenv");
const customerRoutes = require('./routes/customerRoute');
const productRoutes = require('./routes/productRoute');
const orderRoutes = require('./routes/orderRoute');

// Load ENV variable
dotenv.config({ path: "./config/config.env" });

const app = express();

// Use incoming requests
app.use(express.json());

// Dev Logging Middleware
if (process.env.NODE_ENV === "development") app.use(morgan("dev"));

// mounting routes
app.use("/", customerRoutes);
app.use("/api", productRoutes);
app.use("/", orderRoutes);

module.exports = app;
