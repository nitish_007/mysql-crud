const Order = require("../models/Orders");
const Product = require("../models/Products");
const Customer = require("../models/Customers");

// add order
exports.addOrder = async (req, res) => {
  try {
    const orderData = {
      CustomerId: req.body.CustomerId,
      ProductId: req.body.ProductId,
    };

    let product = await Product.findOne({
      where: {
        id: req.body.ProductId,
      },
    });

    let customer = await Customer.findOne({
      where: {
        id: req.body.CustomerId,
      },
    });

    if (!product || !customer) {
      return res
        .status(403)
        .json({ status: 404, error: "Customer or Product is not available!!" });
    }

    const orderCreated = await Order.create(orderData);

    product = await Product.findByPk(req.body.ProductId);

    purchaseCount = product.purchaseCount + 1;

    await Product.update(
      { purchaseCount },
      {
        where: {
          id: req.body.ProductId,
        },
      }
    );

    res.status(201).json({
      status: 201,
      msg: "order created",
      result: orderCreated,
    });
  } catch (error) {
    res.status(404).json({ error });
  }
};

// fetch all orders
exports.fetchAllOrder = async (req, res) => {
  try {
    const orders = await Order.findAll();
    if (!orders || orders.length < 1) {
      return res.status(400).json({ status: 400, msg: "No Order available" });
    }
    res.status(200).json({
      status: 200,
      msg: "orders fetched",
      ordersCount: orders.length,
      orders,
    });
  } catch (error) {
    res.status(404).json({ error });
  }
};

// fetch single orders
exports.fetchSingleOrder = async (req, res) => {
  try {
    const id = req.params.id;
    const order = await Order.findByPk(id);
    if (!order) {
      return res.status(400).json({ status: 400, msg: "No order available" });
    }
    res.status(200).json({
      status: 200,
      msg: "order fetched",
      order,
    });
  } catch (error) {
    res.status(404).json({ error });
  }
};

//update order
exports.updateOrder = async (req, res) => {
  try {
    const updates = Object.keys(req.body);
    const allowedUpdates = ["CustomerId", "ProductId"];
    const isValidOperation = updates.every((update) =>
      allowedUpdates.includes(update)
    );

    if (!isValidOperation)
      return res.status(400).send({ error: "Invalid Update" });

    const data = {};
    id = req.params.id;

    updates.forEach((update) => {
      data[update] = req.body[update];
    });

    const order = await Order.findByPk(id);
    if (!order) {
      return res.status(404).json({ status: 404, msg: "No Order available" });
    }

    await Order.update(data, {
      where: {
        id,
      },
    });

    res.status(200).json({
      status: 200,
      msg: "Order updated",
    });
  } catch (error) {
    res.status(404).json({ error });
  }
};

// Delete single order
exports.deleteSingleOrder = async (req, res) => {
  try {
    id = req.params.id;

    const order = await Order.findByPk(id);
    if (!order) {
      return res.status(404).json({ status: 404, msg: "No Order available" });
    }

    await Order.destroy({
      where: {
        id,
      },
    });

    res.status(200).json({
      status: 200,
      msg: "Order deleted",
    });
  } catch (error) {
    res.status(404).json({ error });
  }
};

// Delete all order
exports.deleteAllOrder = async (req, res) => {
  try {
    await Order.destroy({
      truncate: true,
    });

    res.status(200).json({
      status: 200,
      msg: "All Orders are deleted",
    });
  } catch (error) {
    res.status(404).json({ error });
  }
};

// fetch the list of all the customer who have purchased a particular item
exports.getCustomerPurchasedParticularProduct = async (req, res) => {
  try {
    const id = req.params.id;
    const order = await Order.findAll({
      where: {
        ProductId: id,
      },
    });

    customersPurchasedProduct = [];
    productsInfo = [];

    for (el of order) {
      const customer = await Customer.findByPk(el.CustomerId);
      const product = await Product.findByPk(el.ProductId);
      modifiedCustomer = {
        name: customer.name,
        email: customer.email,
        age: customer.age,
      };
      modifiedProduct = {
        product_name: product.product_name,
        price: product.price,
      }
      customersPurchasedProduct.push(modifiedCustomer);
      productsInfo.push(modifiedProduct);
    }

    if (!order) {
      return res.status(404).json({ status: 404, msg: "No order available" });
    }
    res.status(200).json({
      status: 200,
      msg: "Customers who have puchased particular product",
      customers: customersPurchasedProduct,
      productInfo: modifiedProduct
    });
  } catch (error) {
    res.status(404).json({ error });
  }
};

// get all products list purchased by a specific customer
exports.getProductPurchasedByCustomer = async (req, res) => {
  try {
    const id = req.params.id;
    const order = await Order.findAll({
      where: {
        CustomerId: id,
      },
    });

    customersPurchasedProduct = [];
    productsInfo = [];

    for (el of order) {
      const customer = await Customer.findByPk(el.CustomerId);
      let product = await Product.findByPk(el.ProductId);
      modifiedProduct = {
        product_name: product.product_name,
        price: product.price,
      };
      modifiedCustomer = {
        name: customer.name,
        email: customer.email,
        age: customer.age,
      };
      productsInfo.push(modifiedProduct);
      customersPurchasedProduct.push(modifiedCustomer);
    }

    if (!order) {
      return res.status(404).json({ status: 404, msg: "No order available" });
    }
    res.status(200).json({
      status: 200,
      msg: "Products puchased by particular customer",
      products: productsInfo,
      customerInfo: customersPurchasedProduct[0]
    });
  } catch (error) {
    res.status(404).json({ error });
  }
};

// get the list of most purchased products in descending order
exports.mostPurchasedProduct = async (req, res) => {
  try {
    const product = await Product.findAll();
    product.sort((a, b) => b.purchaseCount - a.purchaseCount);

    const mostPurchased = [];

    for (el of product) {
      productPurchased = {
        product_name: el.product_name,
        price: el.price,
        purchaseCount: el.purchaseCount,
      };
      mostPurchased.push(productPurchased);
    }

    res.status(200).json({
      status: 200,
      msg: "Most purchased Products",
      products: mostPurchased,
    });
  } catch (error) {
    res.status(404).json({ error });
  }
};
