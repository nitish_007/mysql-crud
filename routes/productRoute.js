const express = require("express");
const router = express.Router();
const {
  addProduct,
  fetchAllProduct,
  fetchSingleProduct,
  updateProduct,
  deleteSingleProduct,
  deleteAllProduct,
} = require("../controllers/productController");

// Insert product
router.post("/products", addProduct);

// Fetch all product
router.get("/products", fetchAllProduct);

// Fetch single product
router.get("/products/:id", fetchSingleProduct);

// Update product
router.patch("/products/:id", updateProduct);

// Delete single product
router.delete("/products/:id", deleteSingleProduct);

// Delete All product
router.delete("/products", deleteAllProduct);

module.exports = router;
