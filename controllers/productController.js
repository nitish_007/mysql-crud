const Product = require("../models/Products");

// add product
exports.addProduct = async (req, res) => {
  try {
    const product = await Product.findOne({
      where: {
        product_name: req.body.product_name,
      },
    });

    if (product) {
      return res
        .status(403)
        .json({ status: 403, error: "Product already exists!!" });
    }

    const productData = {
      product_name: req.body.product_name,
      price: req.body.price,
    };

    const productCreated = await Product.create(productData);
    res.status(201).json({
      status: 201,
      msg: "Product created",
      result: productCreated,
    });
  } catch (error) {
    res.status(404).json({ error });
  }
};

// fetch all products
exports.fetchAllProduct = async (req, res) => {
  try {
    const products = await Product.findAll();
    if (!products || products.length < 1) {
      return res.status(404).json({ status: 404, msg: "No Product available" });
    }
    res.status(200).json({
      status: 200,
      msg: "Products fetched",
      productsCount: products.length,
      products,
    });
  } catch (error) {
    res.status(404).json({ error });
  }
};

// fetch single product
exports.fetchSingleProduct = async (req, res) => {
  try {
    const id = req.params.id;
    const product = await Product.findByPk(id);
    if (!product) {
      return res.status(404).json({ status: 404, msg: "No Product available" });
    }
    res.status(200).json({
      status: 200,
      msg: "Product fetched",
      product,
    });
  } catch (error) {
    res.status(404).json({ error });
  }
};

//update product
exports.updateProduct = async (req, res) => {
  try {
    const updates = Object.keys(req.body);
    const allowedUpdates = ["product_name", "price"];
    const isValidOperation = updates.every((update) =>
      allowedUpdates.includes(update)
    );

    if (!isValidOperation)
      return res.status(400).send({ error: "Invalid Update" });

    const data = {};
    id = req.params.id;

    updates.forEach((update) => {
      data[update] = req.body[update];
    });

    const product = await Product.findByPk(id);
    if (!product) {
      return res.status(404).json({ status: 404, msg: "No Product available" });
    }

    await Product.update(data, {
      where: { id },
    });

    res.status(200).json({
      status: 200,
      msg: "Product updated",
    });
  } catch (error) {
    res.status(404).json({ error });
  }
};

// Delete single product
exports.deleteSingleProduct = async (req, res) => {
  try {
    id = req.params.id;

    const product = await Product.findByPk(id);
    if (!product) {
      return res.status(404).json({ status: 404, msg: "No Product available" });
    }

    await Product.destroy({
      where: { id },
    });

    res.status(200).json({
      status: 200,
      msg: "Product deleted",
    });
  } catch (error) {
    res.status(404).json({ error });
  }
};

// Delete all product
exports.deleteAllProduct = async (req, res) => {
  try {
    await Product.destroy({
      truncate: true,
    });

    res.status(200).json({
      status: 200,
      msg: "All Products are deleted",
    });
  } catch (error) {
    res.status(404).json({ error });
  }
};
