const app = require("./app");
const http = require("http");
const Customer = require("./models/Customers");
const Product = require("./models/Products");
const Order = require("./models/Orders");
const sequelize = require("./config/db");

const PORT = process.env.PORT || 3000;
const server = http.createServer(app);

// Association Relation
Customer.hasMany(Order, { foreignKey: { allowNull: false } });
Product.hasMany(Order, { foreignKey: { allowNull: false } });

Order.belongsTo(Customer, {
  foreignKey: { allowNull: false },
  constraints: true,
  onDelete: "CASCADE",
});
Order.belongsTo(Product, {
  foreignKey: { allowNull: false },
  constraints: true,
  onDelete: "CASCADE",
});

// Sequelize Sync
sequelize
  // .sync({ force: true })
  .sync()
  .then((res) => {
    server.listen(PORT, () => {
      console.log(`server Running on Port ${PORT}`);
    });
  })
  .catch((err) => console.error(err));
