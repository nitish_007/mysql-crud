const express = require("express");
const router = express.Router();
const {
  addCustomer,
  fetchAllcustomer,
  fetchSingleCustomer,
  updatecustomer,
  deleteSingleCustomer,
  deleteAllCustomer,
} = require("../controllers/customerController");

// Insert single customer
router.post("/customers", addCustomer);

// Fetch all customer
router.get("/customers", fetchAllcustomer);

// Fetch single customer
router.get("/customers/:id", fetchSingleCustomer);

// Update customer
router.patch("/customers/:id", updatecustomer);

// Delete single customer
router.delete("/customers/:id", deleteSingleCustomer);

// Delete All customer
router.delete("/customers", deleteAllCustomer);

module.exports = router;
