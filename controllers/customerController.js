const Customer = require("../models/Customers");

// add customer
exports.addCustomer = async (req, res) => {
  try {
    const customer = await Customer.findOne({
      where: {
        email: req.body.email,
      },
    });

    if (customer) {
      return res
        .status(403)
        .json({ status: 403, error: "Customer already exists!!" });
    }

    const customerData = {
      name: req.body.name,
      email: req.body.email,
      age: req.body.age,
    };

    const customerCreated = await Customer.create(customerData);
    res.status(201).json({
      status: 201,
      msg: "customer created",
      result: customerCreated,
    });
  } catch (error) {
    res.status(404).json({ error });
  }
};

// fetch all customers
exports.fetchAllcustomer = async (req, res) => {
  try {
    const customers = await Customer.findAll();
    if (!customers || customers.length < 1) {
      return res
        .status(404)
        .json({ status: 404, msg: "No Customer available" });
    }
    res.status(200).json({
      status: 200,
      msg: "Customers fetched",
      customersCount: customers.length,
      customers,
    });
  } catch (error) {
    res.status(404).json({ error });
  }
};

// fetch single customers
exports.fetchSingleCustomer = async (req, res) => {
  try {
    const id = req.params.id;
    const customer = await Customer.findByPk(id);
    if (!customer) {
      return res
        .status(404)
        .json({ status: 404, msg: "No Customer available" });
    }
    res.status(200).json({
      status: 200,
      msg: "Customer fetched",
      customer,
    });
  } catch (error) {
    res.status(404).json({ error });
  }
};

//update customer
exports.updatecustomer = async (req, res) => {
  try {
    const updates = Object.keys(req.body);
    const allowedUpdates = ["name", "email", "age"];
    const isValidOperation = updates.every((update) =>
      allowedUpdates.includes(update)
    );

    if (!isValidOperation)
      return res.status(400).send({ error: "Invalid Update" });

    const data = {};
    id = req.params.id;

    updates.forEach((update) => {
      data[update] = req.body[update];
    });

    const customer = await Customer.findByPk(id);
    if (!customer) {
      return res
        .status(400)
        .json({ status: 404, msg: "No Customer available" });
    }

    await Customer.update(data, {
      where: { id },
    });

    res.status(200).json({
      status: 200,
      msg: "CustomerInfo updated",
    });
  } catch (error) {
    res.status(404).json({ error });
  }
};

// Delete single Customer
exports.deleteSingleCustomer = async (req, res) => {
  try {
    id = req.params.id;

    const customer = await Customer.findByPk(id);
    if (!customer) {
      return res
        .status(400)
        .json({ status: 404, msg: "No Customer available" });
    }

    await Customer.destroy({
      where: { id },
    });

    res.status(200).json({
      status: 200,
      msg: "Customer deleted",
    });
  } catch (error) {
    res.status(404).json({ error });
  }
};

// Delete all Customer
exports.deleteAllCustomer = async (req, res) => {
  try {
    await Customer.destroy({
      truncate: true,
    });

    res.status(200).json({
      status: 200,
      msg: "All Customers are deleted",
    });
  } catch (error) {
    res.status(404).json({ error });
  }
};
