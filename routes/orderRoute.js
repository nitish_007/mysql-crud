const express = require("express");
const router = express.Router();
const {
  addOrder,
  fetchAllOrder,
  fetchSingleOrder,
  updateOrder,
  deleteSingleOrder,
  deleteAllOrder,
  getCustomerPurchasedParticularProduct,
  getProductPurchasedByCustomer,
  mostPurchasedProduct
} = require("../controllers/orderController");


// Insert Order
router.post("/orders", addOrder);

// Fetch all Order
router.get("/orders", fetchAllOrder);

// get the list of most purchased products in descending order
router.get("/orders/mostpurchasedproducts", mostPurchasedProduct);

// fetch the list of all the customer who have purchased a particular item
router.get("/orders/customer/:id", getCustomerPurchasedParticularProduct);

// get the list of most purchased products in descending order.
router.get("/orders/product/:id", getProductPurchasedByCustomer);

// Fetch single Order
router.get("/orders/:id", fetchSingleOrder);

// Update Order
router.patch("/orders/:id", updateOrder);

// Delete single Order
router.delete("/orders/:id", deleteSingleOrder);

// Delete All Order
router.delete("/orders", deleteAllOrder);

module.exports = router;
